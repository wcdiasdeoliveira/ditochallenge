package br.com.demoapp.autocomplete.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WordToCompleteEvent extends BaseEvent{

	private final static long serialVersionUID = 3l;	
	private String text;
	private ArrayList<String> relatedWords;
	
	public WordToCompleteEvent(@JsonProperty("event")String name, @JsonProperty("timestamp")String timestamp, @JsonProperty("text")String text) {
		super(name, timestamp);
		this.setText(text);
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}

	public ArrayList<String> getRelatedWords() {
		return relatedWords;
	}
	
	public void setRelatedWords(ArrayList<String> relatedWords) {
		this.relatedWords = relatedWords;
	}
}