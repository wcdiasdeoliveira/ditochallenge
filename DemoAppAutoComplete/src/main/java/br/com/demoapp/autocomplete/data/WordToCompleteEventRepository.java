package br.com.demoapp.autocomplete.data;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import br.com.demoapp.autocomplete.model.WordToCompleteEvent;

@Repository
public class WordToCompleteEventRepository {
	
	@Value("${word.search.nebula}")
	private String wordSearchNebula;
	
	@Autowired
	private StringRedisTemplate redisTemplate;
	
	public void save(WordToCompleteEvent wordToCompleteEvent){
		redisTemplate.opsForSet().add(wordSearchNebula + "|" + wordToCompleteEvent.getText().substring(0, 2).toLowerCase(), wordToCompleteEvent.getText());
	}
	
	public ArrayList<String> findByOccurence(WordToCompleteEvent wordToCompleteEvent){
		ArrayList<String> relatedWords = new ArrayList<>();
		Set<String> result = redisTemplate.opsForSet().members(wordSearchNebula + "|" + wordToCompleteEvent.getText().substring(0, 2).toLowerCase());
		result.forEach(res -> {
			relatedWords.add((String)res);
		});
		return relatedWords;
	}
}