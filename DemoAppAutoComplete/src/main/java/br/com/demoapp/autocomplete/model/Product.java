package br.com.demoapp.autocomplete.model;

import java.io.Serializable;

public class Product implements Serializable{
	
	private final static long serialVersionUID = 5l;
	private String name;
	private int price;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	} 
}