package br.com.demoapp.autocomplete.model;

import java.io.Serializable;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseEvent extends ResourceSupport implements Serializable{
	
	private final static long serialVersionUID = 1l;
	private String name;
	private String timestamp;
	
	public BaseEvent() {}
	
    @JsonCreator
	public BaseEvent(@JsonProperty("event")String name, @JsonProperty("timestamp")String timestamp) {
		this.setName(name);
		this.setTimestamp(timestamp);
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
}