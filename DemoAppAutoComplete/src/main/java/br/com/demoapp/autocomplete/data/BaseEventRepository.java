package br.com.demoapp.autocomplete.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.demoapp.autocomplete.model.BaseEvent;

@Repository
public class BaseEventRepository {
	
	@Value("${event.queue}")
	private String eventQueue;
	
	@Autowired
	private StringRedisTemplate redisTemplate;
	
	public void save(BaseEvent baseEvent){
		ObjectMapper mapper = new ObjectMapper();		
		try {
			redisTemplate.opsForList().leftPush(eventQueue, mapper.writeValueAsString(baseEvent));	
		}
		catch (Exception ignored) {}    	
	}
}