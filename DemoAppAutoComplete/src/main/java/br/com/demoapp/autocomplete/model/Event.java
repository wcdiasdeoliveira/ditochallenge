package br.com.demoapp.autocomplete.model;

import java.util.ArrayList;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Event extends BaseEvent{
	
	private final static long serialVersionUID = 2l;
	private Double revenue;
	private ArrayList<HashMap<String, String>> customData;
    
    @JsonCreator
	public Event(@JsonProperty("event")String name, @JsonProperty("timestamp")String timestamp, 
			@JsonProperty("revenue")Double revenue,
			@JsonProperty("custom_data")ArrayList<HashMap<String, String>> customData ) {
    	super(name, timestamp);
		this.setName(name);
		this.setTimestamp(timestamp);
		this.setRevenue(revenue);
		this.setCustomData(customData);
	}
	
	public Double getRevenue() {
		return revenue;
	}
	
	public void setRevenue(Double revenue) {
		this.revenue = revenue;
	}
	
	public ArrayList<HashMap<String, String>> getCustomData() {
		return customData;
	}
	
	public void setCustomData(ArrayList<HashMap<String, String>> customData) {
		this.customData = customData;
	}	
}