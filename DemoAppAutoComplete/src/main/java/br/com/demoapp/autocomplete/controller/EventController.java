package br.com.demoapp.autocomplete.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.demoapp.autocomplete.data.BaseEventRepository;
import br.com.demoapp.autocomplete.data.WordToCompleteEventRepository;
import br.com.demoapp.autocomplete.model.BaseEvent;
import br.com.demoapp.autocomplete.model.Event;
import br.com.demoapp.autocomplete.model.Product;
import br.com.demoapp.autocomplete.model.TimeLineEvent;
import br.com.demoapp.autocomplete.model.WordToCompleteEvent;
import net.minidev.json.JSONObject;

@RestController
public class EventController {
	
	@Autowired
	private BaseEventRepository eventRepository;
	
	@Autowired
	private WordToCompleteEventRepository wordToCompleteEventRepository;
	
    @RequestMapping("/event")
	public HttpEntity<BaseEvent> saveEvent(@RequestParam(value = "event", required = true) String name, @RequestParam(value = "timestamp", required = true) String timestamp){
    	BaseEvent event = new BaseEvent(name, timestamp);
    	eventRepository.save(event);
		event.add(linkTo(methodOn(EventController.class).saveEvent(name, timestamp)).withSelfRel());
        return new ResponseEntity<>(event, HttpStatus.OK);
	}
    
    @RequestMapping("/wordtocomplete")
	public HttpEntity<BaseEvent> wordToComplete(@RequestParam(value = "event", required = true) String name, 
			@RequestParam(value = "timestamp", required = true) String timestamp,
			@RequestParam(value = "text", required = true) String text){
    	WordToCompleteEvent event = new WordToCompleteEvent(name, timestamp, text);
    	wordToCompleteEventRepository.save(event);
    	event.setRelatedWords(wordToCompleteEventRepository.findByOccurence(event));
		event.add(linkTo(methodOn(EventController.class).wordToComplete(name, timestamp, text)).withSelfRel());
        return new ResponseEntity<>(event, HttpStatus.OK);
	}
    
    private TimeLineEvent combineInnerFields(Event event) {
    	TimeLineEvent result = new TimeLineEvent();
    	result.setTimestamp(event.getTimestamp());
    	result.setRevenue(event.getRevenue());
    	Product product = new Product();
    	boolean isMixedData = false;
    	boolean isProductData = false;
    	for (HashMap<String, String> customData : event.getCustomData()) {
    		Object customDataValue = customData.get("value");
        	isMixedData = customData.values().contains("product_price") &&
        			customData.values().contains("product_name") &&(
        			customData.values().contains("store_name") ||
        			customData.values().contains("transaction_id"));
        	isProductData = !isMixedData;
    		if(customData.values().contains("product_name")) {
    			product.setName(
    					String.valueOf(customDataValue)
    			);
			}    			
			else if(customData.values().contains("product_price")) {
				product.setPrice(
    					Integer.valueOf(String.valueOf(customDataValue))
    			);
			}
			else if(customData.values().contains("store_name")) {
				result.setStoreName(
						String.valueOf(customDataValue)
				);
			} 	    
			else if(customData.values().contains("transaction_id")){
				result.setTransactionId(
						String.valueOf(customDataValue)
				);
			}  
    	}
    	if(isProductData) {
    		result.getProducts().add(product);
    	}    	
    	return result;
    }
    
    private List<TimeLineEvent> groupTimeLineEvents(List<TimeLineEvent> timeLineEvents){
    	List<TimeLineEvent> result = new ArrayList<TimeLineEvent>();
    	HashMap<String, TimeLineEvent> grouped = new HashMap<String, TimeLineEvent>();
    	for (TimeLineEvent timeLineEvent : timeLineEvents) {
    		String key = timeLineEvent.getTransactionId();    		
    		if(!grouped.containsKey(key)) {
    			grouped.put(key, timeLineEvent);
    		}else { 
    			if(timeLineEvent.getProducts().get(0).getPrice() > 0) {   	
        			grouped.get(key).getProducts().addAll(timeLineEvent.getProducts());	
    			}
    		}
    		if(timeLineEvent.getStoreName() != null && grouped.get(key).getStoreName() == null) {
    			grouped.get(key).setStoreName(timeLineEvent.getStoreName());
    		}    		
		}
    	result.addAll(grouped.values());
    	result.stream().forEach(event -> {
    		event.setRevenue(
    				event.getProducts().stream().mapToDouble(product -> product.getPrice()).sum()
    		);
    	});
    	return result;
    }
   
    private List<TimeLineEvent> prepareTimeLine(List<Event> eventsToGroup){
    	List<TimeLineEvent> result = eventsToGroup.stream().map(event -> this.combineInnerFields(event) ).collect(Collectors.toList());
    	result = this.groupTimeLineEvents(result);
    	Collections.reverse(result);
    	return result;
    }
    
    @RequestMapping("/listdata")
	public HttpEntity<JSONObject> listData(){  
	    RestTemplate restTemplate = new RestTemplate();
	    ObjectMapper mapper = new ObjectMapper();
        ResponseEntity<JSONObject> rawResponse = restTemplate.exchange(
        		"https://storage.googleapis.com/dito-questions/events.json", HttpMethod.GET, null, new ParameterizedTypeReference<JSONObject>() {}
	    );
	    List<Event> result = new ArrayList<Event>();
	    try {
	    	result = mapper.readValue(mapper.writeValueAsString((ArrayList)rawResponse.getBody().get("events")), new TypeReference<List<Event>>(){});
		} catch (Exception e) {
			e.printStackTrace();
		}
	    JSONObject jsonResult = new JSONObject(); 
	    jsonResult.put("timeline", this.prepareTimeLine(result));
        return new ResponseEntity<>(jsonResult, HttpStatus.OK);
	}
}