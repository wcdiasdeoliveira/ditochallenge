package br.com.demoapp.autocomplete.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

@EnableAutoConfiguration
public class JedisConfig {
	
	 @Bean
	  public JedisConnectionFactory redisConnectionFactory() {
	    return new JedisConnectionFactory();
	  }
}