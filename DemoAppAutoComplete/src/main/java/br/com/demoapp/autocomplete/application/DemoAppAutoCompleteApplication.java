package br.com.demoapp.autocomplete.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages= {"br.com.demoapp.autocomplete"})
@EnableAutoConfiguration
public class DemoAppAutoCompleteApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoAppAutoCompleteApplication.class, args);
	}
}
