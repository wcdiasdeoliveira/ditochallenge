package br.com.demoapp.autocomplete.model;

import java.io.Serializable;
import java.util.ArrayList;

public class TimeLineEvent implements Serializable{
	
	private final static long serialVersionUID = 4l;
	private String timestamp;
	private Double revenue;
	private String transactionId;
	private String storeName;
	private ArrayList<Product> products;
	
	public TimeLineEvent() {
		this.setProducts(new ArrayList<>());
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	public Double getRevenue() {
		return revenue;
	}
	
	public void setRevenue(Double revenue) {
		this.revenue = revenue;
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getStoreName() {
		return storeName;
	}
	
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	
	public ArrayList<Product> getProducts() {
		return products;
	}
	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}
}