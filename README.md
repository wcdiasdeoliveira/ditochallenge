# DitoChallenge

## Synopsis

Hello!

This is just an demo app, only to comprove some skills. 

It's a simple project containning one artefact: DitoChallenge.

The application it's restful(HATEOAS) based and use REDIS( https://redis.io/ ) as a NOSQL database to store some info.

According to the proposed test, the application should complete three main tasks:

I - Receive and save events in an endpoint;

II - Implement an endpoint to a autocomplete service;

III - Consume an endpoint and order it's result using the timestamp value;

You can check the services listed down below:

I - /event

II - /wordtocomplete

III - /listdata

I did not pretend to develop all validations and features that that kind of app require but it seems to run really good 
and has a minimum test coverage.

## Installation

Clone the repository:

git clone https://github.com/wcdoliveira/ditochallenge

Inside the respective folder, run:

./gradlew build && java -jar build/libs/<APP_TO_RUN>.jar OR ./gradlew build run

## Contributors

Wesley C. Dias de Oliveira

## License

Free for use and extend.
